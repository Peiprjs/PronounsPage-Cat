-- Up

CREATE TABLE social_lookup (
    userId TEXT NOT NULL REFERENCES users ON DELETE CASCADE,
    provider TEXT NOT NULL,
    identifier TEXT NOT NULL
);

CREATE INDEX "social_lookup_provider_identifier" ON "social_lookup" ("provider", "identifier");

-- Down
