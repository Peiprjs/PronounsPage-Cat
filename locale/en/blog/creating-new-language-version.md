# How to create a new language version of Pronouns.page

<small>2020-12-06 | [@andrea](/@andrea)</small>

So you're interested in helping us extend the platform to your native language? Awesome! 🥰

But how do I do that, you ask?

First, please [contact us](/contact).
Maybe someone already started working on that language?
It would be useful to coordinate before you put work into it.
We'll invite you to our Discord server and offer support.

If you know how to work with tools like Git, Yarn, JavaScript etc.,
you can head to [the source code](https://gitlab.com/PronounsPage/PronounsPage/),
clone the repository and set up the project according to the instruction in `README.md`.
You'll be able to see the changes in real time as you make them.
Push them to a separate branch and submit a pull request.

Otherwise, **don't worry about code**. You can still help localise the project without knowing how to code.
I'll prepare all the necessary files and share them in the pinned message in a dedicated Discord channel – you can just start editing.

Alternatively:
Click [here](https://gitlab.com/PronounsPage/PronounsPage/-/archive/main/Zaimki-main.zip) to download a zip file with the code.
Don't mind everything else, just create a copy of the `/locale/_base` directory,
calling the copy according to the new language code.
Instead of `_base` (a version of English modified for this purpose) you can use a different language as a base,
if you think it's gonna be easier – 
eg. for translating into Slavic languages it might be better to start with `pl` (Polish).

Inside that directory hides all the config and code related to the specific language.
The most important files are:

## [translation.suml](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/translations.suml)

This file contains translation keys with corresponding values.
Basically, keep everything before the colon as it is, and translate everything after the colon into your language.

Or rather **localise it, not translate**. Every language is unique, so remember to _adjust_
the content to whatever makes more sense, not just say the same thing just in another language.
For example, your language might not only have its pronouns gendered, but also other forms –
in that case don't just translate the sentences that use the word “pronouns” as “pronouns”,
but more like “gendered forms”. Another example: there's a paragraph where we compare respecting someone's pronouns
to respecting their names, in which we use the names “Ashley” and “Samantha” –
please use instead some names that are popular in your culture.
Basically, feel free to add, remove and edit stuff in order to make the content
as useful, fitting and relevant to your language as possible.

Also, remember that not everything in this file will necessarily require a translation from you.
For instance, the Polish locale contains translations for the “names” module,
which a new language will probably not have right away (although it might be added later).

## [config.suml](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/config.suml)

Although this file is very important, I don't expect you to handle it, if you're not a technical person.
Still, some translations are also included there, so please go through it and see if you can do something useful there.
The format should be pretty intuitive.

This file contains for example a definition of [links](/links), [academic papers](https://zaimki.pl/nauka) etc.
You can either adjust those definitions directly in this file if you can,
or you can just put them in some Word or Excel file for me to handle.

## [pronouns.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/pronouns.tsv), [pronounGroups.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/pronounGroups.tsv), [examples.tsv](https://gitlab.com/PronounsPage/PronounsPage/-/blob/main/locale/_base/pronouns/examples.tsv)

In this context, by “pronouns” we mean any forms that are gendered in your language,
be it actual pronouns or something like endings of verbs or adjectives, etc. –
as long as they're regular. By “regular” we mean that it's possible to put some rules into a table that can be applied
to all (or most) similar words; eg. Spanish: querid**a** – querid**o**; rápid**a** – rápid**o**, etc.;
as opposed to for example English nouns: chair**woman** – chair**man**; wait**ress** – wait**er**; **wife** – **husband**, etc. –
for them, it's better to use the dictionary section where a more complex database can be managed more dynamically.

You can edit the `.tsv` files in Excel, Numbers etc., or even just a text editor (just keep the tabs where they were).

For building the “pronouns” section, I'd suggest starting with creating some concrete example sentences
that can later be abstracted away into templates that the code can use. For example, in Spanish one sentence might be:

 - Ella es muy querida. _(feminine)_
 - Él es muy querido. _(masculine)_
 - Elle es muy queride. _(neuter)_

Start with just feminine, masculine and optionally neuter forms – we can add any others (neopronouns, etc.) later.
Create as many sentences as necessary to show all the possible (regular) ways that your language can be gendered.

Next, identify, which parts of those sentences differ between gendered forms and which remain the same:

- **Ella** es muy querid**a**. _(feminine)_
- **Él** es muy querid**o**. _(masculine)_
- **Elle** es muy querid**e**. _(neuter)_

Finally, replace those gendered parts with “tags”, “placeholders” that the code will use as blanks to fill in.
Give the tags descriptive names (in English, so that team members like coders who don't speak your language can understand their purpose)
and mark them with `{…}`, for example: `{pronoun_nominative}`, `{adjective_inflection}`…

- {pronoun_nominative} es muy querid{adjective_inflection}.

You can now put all those sentences to `examples.tsv` and start working on `pronouns.tsv`.

Each row in this file represents a pronoun set, like they/them, she/her etc. Add the most popular options there
(all others will be possible for users to create dynamically in the generator).

Columns `key` contains “keys”, unique identifiers that the code will look up based on the URL.
For example, if the key is `they,they/them` then both links [en.pronouns.page/**they**](https://en.pronouns.page/they)
and [en.pronouns.page/**they/them**](https://en.pronouns.page/they/them) will identify that row as the one to be used on that page.
Columns with values of `TRUE` and `FALSE` should stay in English because they're supposed to be understood by the code,
not shown to the visitors. Columns in between `normative` and `plural` should be added/removed in such a way that
each of the “tags” that you've identified in the previous step has one column for itself.

Also keep in mind that, again, it's about localising not translating, so don't just copy-paste English neopronouns,
unless they're actually being used in your language too, instead add what's actually used or proposed.
Columns like `history` should contain extra info about how the form originated in your language,
and not be a translation of the English singular they's history, etc. etc.

In our simplified Spanish example, the `pronouns.tsv` table would look like this:

{wide_table}

| key          | description                | normative | pronoun_nominative | adjective_inflection | plural | pluralHonorific | pronounceable | history | thirdForm | smallForm | sourcesInfo |
|--------------|----------------------------|-----------|--------------------|----------------------|--------|-----------------|---------------|---------|-----------|--------------|--------------|
| ella,ella/la | Femenino                   | TRUE      | ella               | a                    | FALSE  | FALSE           | TRUE          |         |           |           |             |
| él,él/lo     | Masculino                  | TRUE      | él                 | o                    | FALSE  | FALSE           | TRUE          |         |           |           |             |
| elle,elle/le | Neopronombre neutro “elle“ | FALSE     | elle               | e                    | FALSE  | FALSE           | TRUE          |  Forma neutra, también llamada “lenguaje inclusivo”. Este pronombre es utilizado para referirse a personas no binarias, y también para grupos mixtos en cuanto a género, para no excluir a nadie.  |           |           |             |

{/wide_table}

Lastly, in `pronounGroups.tsv` you can specify which pronouns should be displayed and described together.

## Managing database entries

All the things described above are more or less “static”: configuration, translations, definitions of regular gendered forms, etc.
However, many parts of our website require a more “dynamic” approach, where additions and modifications are frequent
and open to crowdsourcing – things like the dictionary of [gendered nouns](/dictionary), of [queer terminology](/terminology),
of [inclusive language suggestions](/inclusive), etc.

Once a first draft of the “static” parts (even just a translation) is ready,
please ping me ([@andrea](/@andrea)) on Discord,
or send us an email to [contact@pronouns.page](mailto:contact@pronouns.page),
or submit a pull request, etc. – and I'll set up a test server
where you'll be able to see your work in practice as well as start adding the “dynamic” parts directly via the website.

The website also offers an interface to propose missing translations of new features and correct errors in the existing ones.
Please **don't use it until your version gets published**, as it adds more unnecessary work and possible edit conflicts.
In the early stages let's just work on the `translations.suml` file directly.

## And more…

Be warned: creating a new language version is a lot of work 😅
The steps above cover most of it, but keep in mind there might be a few other minor things that might come up.

If you're not discouraged by the effort required, then, well, good luck! And thank you for contributing! 🥰
